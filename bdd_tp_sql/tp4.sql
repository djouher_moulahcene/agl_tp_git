##############################################################
#Exercice 1 tp 4 :
#############################################################

#question 1 :

Desc cat;
select * from cat ;

#question 2 :
#Desc on l'utilise que sur les tables systemes

DESC COLS;
SELECT COUNT(COLUMN_ID) FROM COLS WHERE TABLE_NAME='ABONNE';

#question 3

DESC USER_CONSTRAINTS;
SELECT CONSTRAINT_NAME,STATUS  FROM USER_CONSTRAINTS;

#question 4

SELECT *
FROM ALL_CONSTRAINTS
WHERE CONSTRAINT_NAME LIKE 'PK_%';

#QUESTION 5

DESC COLS;
SELECT *  FROM COLS WHERE TABLE_NAME='ABONNE';

##############################################################
#Exercice 2 tp 4 :
#############################################################
#question 1 :
GRANT SELECT ON ABONNE TO e20200010244;

#question 2 :#ON AJOUT SELECT CAR LE WHERE C'EST CONSIDERER COMME UN SELECT 
GRANT SELECT,UPDATE ON LIVRE TO e20200010244;

#question 3 :
DESC user_tab_privs;
select table_name,owner, privilege from user_tab_privs;

#QUESTION 4 :ON PEUT FAIRE CA QUE SI ON A EU LE DROIT DE MODFIER SUR EXEMPLAIRE 
UPDATE E20200010244.EXEMPLAIRE SET PRIX=5.2  WHERE NUMERO=1010;#SI ON FAIT SEULEMENT CA ON NE FAIT QUE MODIFIER LA BASE SUR LE JOURNAL DE CETTE MACHINE 

cOMMIT; #POUR VALIDER LES MODIFS DANS LA BASE DE DONNÉES 

#QUESTION 5 :

CREATE VIEW MY_TABLE AS  
SELECT * FROM E20200010244.EXEMPLAIRE;
#UNE FOIS LA VUE CRÉE ON PEUT LA MANIPULER NORMALEMENT 
SELECT * FROM MY_TABLE;

##########################################################################
#QUESTION 1 :

REVOKE SELECT ON ABONNE FROM e20200010244;

#QUESTION 2:

select table_name,owner, privilege from user_tab_privs;

#QUESTION 3 :
#LA VUE À DISPARUE PUISQUE SIHEM M'AS ENLEVER LES DROITS 

##############################################################
#Exercice 3 tp 4 :
#############################################################
#QUESTION 1 :IL NE SE PASSE RIEN 
 SELECT NOM,PRENOM FROM ABONNE;

 #QUESTION 2:

UPDATE ABONNE SET NOM='JAMES' WHERE NOM='DUPOND';
SELECT NOM,PRENOM FROM ABONNE;
COMMIT; #(POUR VALIDER LES MODIFS)
ROLLBACK; #POUR ANNULLER LA MODIF FAITE 

#QUESTION 3 :
#À CHAQUE MODIF IL FAUT FAIRE UN COMMIT POUR EVITER D'AVOIR DES CONFLIT 





