/*
USAGE :
Utiliser ce fichier pour répondre aux différentes questions. Penser à remplir les champs Numéro carte, nom, prénom, date.
Pour chaque requete vous avez le résultat que vous devez obtenir.
Les questions sont entre commentaires (sauf la première).
Attention sous ORACLE pour les marques des commentaires (le slash et l'étoile) doivent être seuls sur une ligne.
*/


/*
Numéro de carte étudiant :
Nom :
Prénom :
Date :
*/

/*
Mise en page - Ne pas toucher
*/
CLEAR SCREEN
SET PAGESIZE 30
COLUMN COLUMN_NAME FORMAT A30
SET LINESIZE 300


prompt --- Q1 : Quels sont les noms et prénoms des abonnés domiciliés à Montpellier ?

/*
SELECT NOM,PRENOM FROM ABONNE WHERE VILLE='MONTPELLIER' ;
*/


/*
Résultat attendu :

NOM     PRENOM
------------ ----------
LEVEQUE      PIERRE
DUPONT     MARIE
RENARD     ALBERT
DUPONT     ANTOINE
DUPONT     SYLVIE
DUPONT     JEAN
MEUNIER      LUC
LUCAS     PAUL
REVEST     ANNIE
ANTON     JEANNE

10 lignes selectionnees.

*/

prompt ---- Q2 : Donner toutes les informations sur les exemplaires dont le code de prêt est : « EMPRUNTABLE ».

/*
SELECT * FROM EXEMPLAIRE WHERE CODE_PRET='EMPRUNTABLE';
*/

/*
Résultat attendu :
     NUMERO DATE_ACHAT    PRIX CODE_PRET      ETAT      ISBN
---------- ---------- ---------- -------------------- --------------- ---------------
      1010 10/04/2015      55 EMPRUNTABLE      BON      0_18_47892_2
      1011 10/04/2015      55 EMPRUNTABLE      BON      0_18_47892_2
      1012 20/05/2015     112 EMPRUNTABLE      BON      3_505_13700_5
      2909 30/03/2017      35 EMPRUNTABLE      BON      3_505_13700_5
      2673 15/03/2018      42 EMPRUNTABLE      ABIME      3_505_13700_5
      2711 20/06/2014     270 EMPRUNTABLE      BON      0_8_7707_2
      3016 15/09/2017     420 EMPRUNTABLE      BON      0_201_14439_5
      3702 20/02/2019     210 EMPRUNTABLE      BON      1_22_1721_3
      4111 03/01/2021      48 EMPRUNTABLE      BON      1_22_1721_3
      4203 29/11/2019      35 EMPRUNTABLE      BON      1_104_1050_2
      4204 29/11/2019      35 EMPRUNTABLE      ABIME      1_104_1050_2
      5003 10/06/2020      39 EMPRUNTABLE      BON      1_104_1050_2
      5004 10/06/2020      41 EMPRUNTABLE      BON      0_15_270500_3
      5005 10/06/2020      41 EMPRUNTABLE      BON      0_15_270500_3
      5104 20/12/2017     470 EMPRUNTABLE      BON      0_12_27550_2
      6006 15/02/2021      33 EMPRUNTABLE      BON      0_85_4107_3
      6007 15/02/2021      33 EMPRUNTABLE      BON      0_85_4107_3
      5202 18/10/2020      40 EMPRUNTABLE      BON      0_18_47892_2
      7001 01/09/2016     420 EMPRUNTABLE      BON      2_7296_0040
      1109 30/05/2016     170 EMPRUNTABLE      BON      9_782070_36

20 lignes selectionnees.
*/



prompt ---- Q3 : Donner la liste des ouvrages (ISBN, TITRE, CATEGORIE), dont le titre inclut le mot ‘ROSE’, triée par ordre décroissant de numéro.

/*
SELECT ISBN,TITRE,CATEGORIE FROM LIVRE WHERE TITRE LIKE '%ROSE%' ORDER BY ISBN DESC;
*/

/*
Résultat attendu :
ISBN TITRE   CATEGORIE
--------------- -------------------------------------------------- ----------------
1_22_1721_3 LE NOM DE LA ROSE   ROMAN
0_18_47892_2 UNE ROSE POUR MORRISSON   ROMAN
0_15_270500_3 LE MIRACLE DE LA ROSE   ROMAN
*/


prompt ---- Q4 : Donner la liste des livres (leur titre et catégorie) de toutes les catégories sauf MEDECINE, SCIENCES et LOISIRS. Cette liste sera donnée triée par ordre alphabétique selon la catégorie.

/*
SELECT TITRE,CATEGORIE FROM LIVRE WHERE CATEGORIE NOT IN ('Medecine','SCIENCES','Loisirs');

*/

/*
Résultat attendu :
TITRE   CATEGORIE
-------------------------------------------------- ----------
GODEL ESCHER BACH : LES BRINS D UNE GUIRLANDE   HISTOIRE
LE MUR   NOUVELLE
POESIES COMPLETES   POEME
UNE ROSE POUR MORRISSON   ROMAN
LA PERLE   ROMAN
LE GRAND VESTIAIRE   ROMAN
L ENFANT   ROMAN
LE MIRACLE DE LA ROSE   ROMAN
LE NOM DE LA ROSE   ROMAN

9 lignes selectionnees.

*/



prompt ---- Q5 : Donner toutes les informations sur les emprunts pour lesquels la date de retour effective (attribut D_RET_REEL) n'est pas renseignée dans la base de données.

/*
SELECT * FROM EMPRUNT WHERE D_RET_REEL IS NULL;
*/

/*
Résultat attendu :
    NUM_AB     NUM_EX D_EMPRUNT D_RETOUR  D_RET_REE NB_RELANCE
---------- ---------- --------- --------- --------- ----------
    911007 4204 19-JAN-03 10-FEB-03     1
    902075 2673 15-FEB-03 28-FEB-03
    902075 1010 05-JAN-03 25-JAN-03     1
    921102 6006 20-DEC-02 10-JAN-03     2
*/

prompt ---- Q6 : Donner, pour l'abonné JEAN DUPONT, la liste des exemplaires empruntés (leur numéro et la date d'emprunt), par ordre croissant de date d'emprunt.

/*
SELECT NUM_EX,D_EMPRUNT FROM ABONNE,EMPRUNT WHERE (ABONNE.NUM_AB=EMPRUNT.NUM_AB) AND (NOM='DUPONT') AND (PRENOM='JEAN') ORDER BY D_EMPRUNT;
*/

/*
Résultat attendu :
    NUM_EX D_EMPRUNT
---------- ----------
      5003 10/03/2020
      5005 30/03/2020
      1012 30/03/2020
      5103 17/06/2020
      1109 15/09/2020
      6007 22/12/2020

6 lignes selectionnees.
*/

prompt ------ Q7 : Donner la liste des exemplaires empruntés (leur numéro, code prêt et état) du livre de titre « LE MUR».

/*
SELECT NUMERO,CODE_PRET,ETAT FROM EXEMPLAIRE,LIVRE WHERE (EXEMPLAIRE.ISBN=LIVRE.ISBN) AND (TITRE='LE MUR');
*/

/*
Résultat attendu :
  NUMERO CODE_PRET ETAT
---------- -------------------- ---------------
      5003 EMPRUNTABLE BON
      4203 EMPRUNTABLE BON
      4204 EMPRUNTABLE ABIME

*/

prompt  ---- Q8 : Quels sont les exemplaires (numéro) reproduisant le même livre que l'exemplaire de numéro 4112 et dont l'état est : « BON » ?

/*

SELECT NUMERO FROM EXEMPLAIRE,LIVRE WHERE (EXEMPLAIRE.ISBN=LIVRE.ISBN) AND TITRE IN (SELECT TITRE FROM EXEMPLAIRE,LIVRE WHERE 
(EXEMPLAIRE.ISBN=LIVRE.ISBN) AND (NUMERO='4112') AND (ETAT='BON') ) AND EXEMPLAIRE.NUMERO<> 4112;

/*
Résultat attendu :
    NUMERO
----------
      3702
      3703
      4111

*/
/*********************************************************************************************************
QUESTION 12 TP 1 :
/***********************************************
/* "TOUS LES ....." ON PEUT LE REPRÉSENTER AVEC UN GROUP BY , IL FAUT JAMAIS OUBLIER LE DISTINCT QUAND LA REQUETE EST SOUS CETTE FORME
À DROITE ON MET LES TOUT LES " (SELECT COUNT(*) FROM SERVICE) "
À GAUCHE IL FAUT SELECTIONNER LES DISTINCTS
/************************************************************************************
SELECT NOM,PRENOM FROM VOISIN 
WHERE NOT EXISTS (SELECT * FROM SERVICE WHERE NOT EXISTS 
(SELECT * FROM TRAVAIL WHERE (IDSERV=IDSERVICE) AND (IDVOIS=IDVOISIN)));






SELECT NOM,PRENOM
FROM VOISIN
JOIN TRAVAIL ON IDVOISIN=IDVOIS
GROUP BY IDVOISIN,NOM,PRENOM
HAVING COUNT(DISTINCT IDSERV) = (SELECT COUNT(*) FROM SERVICE) ;
/*********************************************************************************************************

prompt ---- Q9 : Existe-t-il une catégorie pour laquelle aucun livre n'a été emprunté ?

/*
SELECT CATEGORIE FROM LIVRE WHERE CATEGORIE NOT IN (SELECT CATEGORIE FROM EMPRUNT,LIVRE,EXEMPLAIRE WHERE 
(LIVRE.ISBN=EXEMPLAIRE.ISBN) AND (EXEMPLAIRE.NUMERO=EMPRUNT.NUM_EX) );
*/


/*
/*reponse 11 du tp 1
select libellé ,v.idvoisin,v.nom,v.prenom,t.prix 
from voisin V join travail T on v.idvoisin=T.idvois join service on idvoisin
Résultat attendu :
CATEGORIE
----------
POEME

*/



prompt ---- Q10 : Quel est le nombre d'emprunt en cours de l'abonné Renard Albert ?

/*
SELECT COUNT(*) FROM EMPRUNT,ABONNE WHERE (ABONNE.NUM_AB=EMPRUNT.NUM_AB) AND (NOM='RENARD') AND (PRENOM='ALBERT') AND (D_RET_REEL IS NULL);
*/

/*
Résultat attendu :
  COUNT(*)
----------
2

*/


prompt ---- Q11 : Quelle est la catégorie de livres pour laquelle l'exemplaire le plus cher a été acheté ?

/*
SELECT CATEGORIE FROM EXEMPLAIRE,LIVRE WHERE (LIVRE.ISBN=EXEMPLAIRE.ISBN) AND PRIX = (SELECT MAX(PRIX) FROM EXEMPLAIRE);
*/

/*
Résultat attendu :
CATEGORIE
----------------
ROMAN

*/

prompt ---- Q12 : Existe-t-il des exemplaires dans l'état « Abimé » et qui sont actuellement empruntés ? Si oui, donner leur numéro.

/*
SELECT NUMERO FROM EXEMPLAIRE,EMPRUNT WHERE (EXEMPLAIRE.NUMERO=EMPRUNT.NUM_EX) AND (ETAT='ABIME') AND (D_RET_REEL IS NULL);
*/

/*
Résultat attendu :
    NUMERO
----------
      2673
      4204
*/

prompt -- Q13 : Existe-t-il des mots clefs ne caractérisant aucun livre ?

/*
SELECT MOT FROM MOT_CLEF WHERE MOT NOT IN (SELECT MOT FROM CARACTERISE );
*/

/*
Résultat attendu :
MOT
--------------------
ESSAI
MEDECINE
NOUVELLE

*/

prompt --- Q14 : Donner le nombre d'emprunts effectués par chacun des abonnés (numéro, nom) pour chacune des catégories de livres proposées.

/*
SELECT ABONNE.NUM_AB,NOM,CATEGORIE,COUNT(*) FROM ABONNE,EMPRUNT,LIVRE,EXEMPLAIRE WHERE (EXEMPLAIRE.NUMERO=EMPRUNT.NUM_EX) AND 
(LIVRE.ISBN=EXEMPLAIRE.ISBN ) AND (ABONNE.NUM_AB=EMPRUNT.NUM_AB) GROUP BY ABONNE.NUM_AB,NOM,CATEGORIE HAVING COUNT(*) >= ALL 
(SELECT COUNT(*) FROM EMPRUNT GROUP BY EMPRUNT.NUM_AB);



=> là c'est une requete corrélative (là corrélation ce fait sur les abonnées )
SELECT A1.NUM_AB,A1.NOM,L1.CATÉGORIE,COUNT(*) FROM ABONNE A1,LIVRE L1,EMPRUNT E1,EXEMPLAIRE E WHERE (A1.NUM_AB=E1.NUM_AB) AND (L1.ISBN=E.ISBN)
AND (NUMERO=NUM_EX) AND L1.CATEGORIE .????????????????????????????


*/

/*
Résultat attendu :
    NUM_AB NOM   CATEGORIE      COUNT(*)
---------- --------------- ---------------- ----------
    911023 DUPONT   NOUVELLE     2
    921102 LUCAS   NOUVELLE     2
    902043 DUPONT   HISTOIRE     2
    902075 RENARD   NOUVELLE     1
    922016 MEUNIER   NOUVELLE     1
    932010 ANTON   NOUVELLE     1
    901001 LEVEQUE   NOUVELLE     2
    911007 MARTIN   ROMAN     3
    911023 DUPONT   SCIENCES     1
    921102 LUCAS   ROMAN     2
    922143 REVEST   NOUVELLE     1
    901001 LEVEQUE   HISTOIRE     2
    911007 MARTIN   NOUVELLE     3
    911021 DUPONT   ROMAN     1
    911021 DUPONT   NOUVELLE     1
    911023 DUPONT   ROMAN     4
    902043 DUPONT   NOUVELLE     1
    901001 LEVEQUE   ROMAN     1
    902043 DUPONT   SCIENCES     2
    902075 RENARD   ROMAN     2
    911022 DUPONT   NOUVELLE     1

21 lignes selectionnees.

*/

prompt --- Q15 : Donner, pour chaque livre (numéro ISBN) ayant plus de deux exemplaires, le prix moyen d'achat des exemplaires.


SELECT AVG(PRIX),LIVRE.ISBN,TITRE FROM LIVRE,EXEMPLAIRE 
WHERE (LIVRE.ISBN=EXEMPLAIRE.ISBN) GROUP BY LIVRE.ISBN,TITRE HAVING COUNT(*) > 2 ;



/*
Résultat attendu :
ISBN     TITRE AVG(PRIX)
-------------------- -------------------------------------------------- ----------
0_18_47892_2     UNE ROSE POUR MORRISSON 50
9_782070_36     LA PERLE 156.666667
3_505_13700_5     LE GRAND VESTIAIRE 63
1_104_1050_2     LE MUR 36.3333333
1_22_1721_3     LE NOM DE LA ROSE       129
2_7296_0040     GODEL ESCHER BACH : LES BRINS D UNE GUIRLANDE       420

6 rows selected.


*/

prompt --- Q16 : Quel est l’abonné ayant effectué le plus d’emprunts ?

/*
c'est sous forme "le plus de <nom table" donc in utilise un having count(*) >= all (select count(*) from ...... group by idemprunt)
*/

SELECT ABONNE.NUM_AB,ABONNE.NOM FROM ABONNE,EMPRUNT 
WHERE (ABONNE.NUM_AB=EMPRUNT.NUM_AB) GROUP BY ABONNE.NUM_AB,ABONNE.NOM 
HAVING COUNT(*) >= ALL(SELECT COUNT(*) FROM EMPRUNT GROUP BY EMPRUNT.NUM_AB);



/*
Résultat attendu :
    NUM_AB NOM
---------- ---------------
    911023 DUPONT
    911007 MARTIN
*/

prompt --- Q17 : Donnez, pour chaque livre (numéro ISBN et titre) emprunté au moins deux fois, son nombre d’exemplaires en catégorie "Exclu".


/*
"(SELECT ISBN FROM EXEMPLAIRE,EMPRUNT WHERE (EMPRUNT.NUM_EX=EXEMPLAIRE.NUMERO) GROUP BY ISBN HAVING COUNT(*)>=2 ) " => là on séléctionne que les livre dont les isbn à été emprunter au moins 2 fois "
ca donne tout le compte d'exemplaire selon isbn => "livre (numéro ISBN et titre) emprunté au moins deux fois"<= 
la requete principale avec le in S'assure que le livre est emprunter au moins 2 fois 
le dernier group by c'est pour chaque livre 

*/

SELECT LIVRE.ISBN,TITRE,COUNT(*) AS "Nombre exemplaire" FROM LIVRE,EXEMPLAIRE
WHERE (EXEMPLAIRE.ISBN=LIVRE.ISBN) AND  LIVRE.ISBN IN 
(SELECT ISBN FROM EXEMPLAIRE,EMPRUNT WHERE (EMPRUNT.NUM_EX=EXEMPLAIRE.NUMERO) GROUP BY ISBN HAVING COUNT(*)>=2 ) 
AND (CODE_PRET='EXCLU') GROUP BY LIVRE.ISBN,TITRE;


/*
Résultat attendu :
ISBN TITRE   Nombre exemplaires
--------------- -------------------------------------------------- ------------------
2_7296_0040 GODEL ESCHER BACH : LES BRINS D UNE GUIRLANDE    1

*/

prompt -- Q18 : Existe t'il des homonymes (NUM_AB, NOM) parmi les abonnés ? Attention dans le résultat il ne faut ne pas dupliquer l'information. Par exemple si deux abonnés s'appellent DUPOND le résultat à obtenir est :
prompt -- 1 DUPOND
prompt -- 2 DUPOND

/*
c'est de l'autojointure avec la table elle même. EXISTS ÉVALUE LE RÉSULTAT SANS RETOURNER LE DIT RÉSULTAT on a plusieurs facons de le faire
*/

/* 1)- avec une requete corrélée : */
SELECT A1.NUM_AB,A1.NOM FROM ABONNE A1 WHERE EXISTS (SELECT * FROM ABONNE A2 WHERE (A1.NUM_AB<>A2.NUM_AB) AND (A1.NOM=A2.NOM));

/* 2)- solution faite en l2 :  */
SELECT A1.NUM_AB,A1.NOM FROM ABONNE  A1 ,ABONNE  A2 WHERE (A1.NOM=A2.NOM) AND (A1.NUM_AB <> A2.NUM_AB) GROUP BY A1.NUM_AB,A1.NOM;

/* 3)- Avec un partitionnement :  */
SELECT NUM_AB,NOM FROM ABONNE WHERE NOM IN (SELECT NOM FROM ABONNE GROUP BY NOM HAVING COUNT(*)>1);

/* 3)- Avec une autre vision de différence (À EVITER ) :  */
SELECT A1.NUM_AB,A1.NOM FROM ABONNE A1,ABONNE A2 WHERE
(A1.NOM=A2.NOM) AND (A1.NUM_AB,A1.NOM,A2.NUM_AB,A2.NOM) NOT IN 
(SELECT A2.NUM_AB,A3.NOM,A4.NUM_AB,A4.NOM FROM ABONNE A3,ABONNE A4 WHERE A1.NUM_AB=A4.NUM_AB);
/*
Résultat attendu :
    NUM_AB NOM
---------- ---------------
    911021 DUPONT
    911022 DUPONT
    911023 DUPONT
    902043 DUPONT
*/


prompt --- Q19 : Existe-t-il des catégories de livres empruntées par tous les abonnés ?

/*
"PAR TOUT LES ......" C'EST UNE DIVISION . 
PARAPHRASE : quelles sont les catégories emprunter par autant d'abonné qu'il en existe ? 

"autant d'abonné qu'il en existe ? " => "select count(*) from abonne " c'est apres le tous les 

"les catégories emprunter" => "select categorie from livre join exemplaire on livre.isbn=exemplaire.isbn 
join emprunt on numero=num_ex group by categorie having count(distinct num_ab) "

"le distinct " => 1 abonne peut emprunter plusieurs fois des exemplaires de la même catégorie .
============>>>>>>>> 

*/

SELECT CATEGORIE FROM LIVRE,EMPRUNT,EXEMPLAIRE 
        WHERE (LIVRE.ISBN=EXEMPLAIRE.ISBN) AND (EXEMPLAIRE.NUMERO=EMPRUNT.NUM_EX) 
                 GROUP BY LIVRE.ISBN,CATEGORIE 
                        HAVING COUNT(DISTINCT EMPRUNT.NUM_AB)= (SELECT COUNT(*) FROM ABONNE);





/* 2 ème facon de faire avec des "not exists" imbriqués "exist-t-il des catégories de livres tel qu'il n'existe aucun abonnés qui n'as 
pas emprunter ce livre" tel que donc le 1er "select " on met le résultat qu'on veux c'est à dire "les categories de livres " donc la 
première partie du "exist-il".
le 2ème "select" porte sur la non existance de la partie 2 de la phrase de base (2eme table) c'est à dire la non existance d'un abonné
qui a emprunter cette catégorie de livre et c'est à cela que porte le 3éme "select" qui s'imbrique dans le 2eme "select" là on modélise
le "non emprunt" où on fait une jointure corréle c'est à dire en fonction des requetes principales .
Le choix des tables de jointures ce fait grâce aux clés primaires des 2 tables du "1er et 2eme select" 

PARAPHRASE : "quelles sont les catégories telles qu'il n'existe aucun abonner n'ayant emprunter un livre de cette catégorie ?"

(SELECT   Resultat )
      NOT EXISTS (SELECT par quoi je divise )
           NOT EXISTS (SELECT une relation qui relie les 2 blocs précédent )

*/

SELECT DISTINCT CATEGORIE FROM LIVRE 
     WHERE NOT EXISTS (SELECT * FROM ABONNE 
       WHERE NOT EXISTS (SELECT * FROM EXEMPLAIRE,EMPRUNT 

            WHERE (EXEMPLAIRE.NUMERO=EMPRUNT.NUM_EX) AND (EXEMPLAIRE.ISBN=LIVRE.ISBN) AND (EMPRUNT.NUM_AB=ABONNE.NUM_AB)));
/*
Résultat attendu :
CATEGORIE
----------------
NOUVELLE



*/

prompt -- Q20 : Quels sont les livres (numéro ISBN et titre) dont tous les exemplaires valent plus de 30 euros ?



 
/*la manière simple c'est : */
SELECT ISBN,TITRE FROM LIVRE WHERE ISBN NOT IN (SELECT DISTINCT ISBN FROM EXEMPLAIRE WHERE PRIX <30 );
  
SELECT LIVRE.ISBN,TITRE FROM LIVRE,EXEMPLAIRE WHERE (LIVRE.ISBN=EXEMPLAIRE.ISBN)
GROUP BY LIVRE.ISBN,TITRE HAVING MIN(PRIX)>30;


/*là on peut aussi le faire selon le paraphrase des "tous les ....." livre dont le NB d'exemplaire valant plus de 30 euro est égal è leur
NB total d'exemplaire  : */

SELECT L.ISBN,TITRE FROM LIVRE L,EXEMPLAIRE E1
WHERE (L.ISBN=E1.ISBN) AND (PRIX>30) GROUP BY L.ISBN,TITRE
HAVING COUNT(*)=(SELECT COUNT(*) FROM EXEMPLAIRE E2 WHERE E2.ISBN=L.ISBN);


/*

*/

/*
Résultat attendu :
ISBN TITRE
--------------- --------------------------------------------------
0_12_27550_2 NEW APPLICATIONS OF DATABASES
0_15_270500_3 LE MIRACLE DE LA ROSE
0_18_47892_2 UNE ROSE POUR MORRISSON
0_201_14439_5 AN INTRODUCTION TO DATABASE SYSTEMS
0_85_4107_3 L ENFANT
1_104_1050_2 LE MUR
2_7296_0040 GODEL ESCHER BACH : LES BRINS D UNE GUIRLANDE
3_505_13700_5 LE GRAND VESTIAIRE

8 lignes selectionnees.






prompt -- Q21 : Quels sont les livres (numéro ISBN et titre) dont tous les exemplaires valant plus de 30 euros ont été 
au moins trois fois empruntés ? 

/*
là on peut utiliser le paraphrase du double not exists puisque c'est un "tous les..." ainsi :
"quels sont les livres (numéro ISBN et titre) tel qu'il n'existe pas d'exemplaire valant plus de 30 euro n'ayant pas était emprunter au 
moins 3 fois  "
*/



SELECT L.ISBN,TITRE FROM LIVRE L
WHERE NOT EXISTS (SELECT * FROM EXEMPLAIRE E WHERE E.ISBN=L.ISBN AND (PRIX>30) AND 
NOT EXISTS (SELECT * FROM EMPRUNT WHERE E.NUMERO=NUM_EX GROUP BY NUM_EX HAVING COUNT(*)>=3));

/*

Résultat attendu :
ISBN TITRE
--------------- --------------------------------------------------
0_112_3785_5 POESIES COMPLETES
0_8_7707_2 BASES DE DONNEES RELATIONNELLES
0_26_28079_6 FUNDAMENTALS OF DATABASE SYSTEMS*/

prompt --- Q22 : Quels sont les livres caractérisés par exactement les mêmes mots clefs que l'ouvrage de numéro ISBN 0-8-7707-2 ? ATTENTION : bien regarder le livre 0_26_28079_6 et ses mots clés.
/*
SELECT LIVRE.ISBN FROM LIVRE,CARACTERISE WHERE (LIVRE.ISBN=CARACTERISE.ISBN) AND MOT IN (SELECT MOT FROM CARACTERISE WHERE ISBN='0-8-7707-2') ;
*/

SELECT L.ISBN FROM LIVRE L,CARACTERISE WHERE L.ISBN=CARACTERISE.ISBN AND NOT EXISTS 
(SELECT MOT FROM CARACTERISE C1 WHERE ISBN='0-8-7707-2' AND NOT EXISTS (SELECT * FROM CARACTERISE C2 WHERE 
    C1.MOT=C2.MOT AND C2.ISBN=L.ISBN)) GROUP BY L.ISBN HAVING COUNT(*)=(SELECT COUNT(*) FROM CARACTERISE WHERE ISBN='0-87707-2');


/*
Résultat attendu :
ISBN
---------------
0_201_14439_5
0_8_7707_2
*/

